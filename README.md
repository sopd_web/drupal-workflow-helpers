# Drupal Workflow Helpers

This repository contains all helpers for Drupal 7/8 Projects bein developed using [this workflow](https://confluence.web.somedia.ch/x/HYJMAg).

It can easily be installed (or updated) using one of these commands:

### For Servers


```bash
SOM_SERVER_INSTALL=1 curl https://bitbucket.org/sopd_web/drupal-workflow-helpers/raw/master/install.bash | bash -s
```

### For Clients (you're not running an instance locally)

```bash
curl https://bitbucket.org/sopd_web/drupal-workflow-helpers/raw/master/install.bash | bash -s
```

## Updating

To update your aliases just run the above command again.

## Extending this repository

You may extend this Repository however you like. But please submit a pull request and to not directly push to the repository. Currently Buno Bless <bruno.bless@somedia.ch> is responsible for mergin pull requests.

If you want to add simple Bash aliases, please do so in `bash/aliases.bash`. All files inside `bash/` are automatically loaded into every Bash that has run the install script. If you want to **add a new function** please create a new file in `bash/`. The file should have be called `[function name].bash` and the function name should be prefixed with `som_`. Function names should generally not contain any abbriviations (but may contain acronyms). You can also create an three letter alias for your function inside your function file. The three letter alias should be prefixed with the letter `s`. To see an example; please refer to `bash/som_update`.

All functions and aliases should be documented [here](https://confluence.web.somedia.ch/x/OIJMAg).

