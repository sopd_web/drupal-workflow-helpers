<?php

// Edit the values wrapped in `[]`
// ROOTPATH = Probably ~/htdocs (exigo/web.somedia) or /var/app (platform)
// HOSTURL = The URL you use to access the front end
// TMPDIR = The directory to save temporary files to. Probably /tmp

$aliases['integration'] = array (
  'root' => '[ROOTPATH]',
  'uri' => '[HOSTURL]',
  'remote-host' => ''[HOSTNAME]',
  'remote-user' => '[USER]',
  'path-aliases' => array(
    '%dump-dir' => '[TMPDIR]',
  ),
);

