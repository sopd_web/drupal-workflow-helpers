DIR=~/drupal-workflow-helpers
RCFILE=""
DRUSH_DIR=~/.bin


if [ -d ${DIR} ]; then
  pushd ${DIR} &> /dev/null
  git pull
  popd &> /dev/null
else
  # Save to .zshrc if there is one (assume it's used as a primary shell)
  if [ -f ~/.zshrc ]; then
    RCFILE=~/.zshrc
  else
    RCFILE=~/.bash_profile
  fi

  git clone -q https://lschmid@bitbucket.org/sopd_web/drupal-workflow-helpers.git ~/drupal-workflow-helpers
  cat << '__EOF__' >> ${RCFILE}
source ~/drupal-workflow-helpers/index.bash
[ -f ~/.bashrc ] && source ~/.bashrc
__EOF__
fi

if [ -n SOM_SERVER_INSTALL -a ! -d ~/.drush ]; then
  pushd ~ &> /dev/null

  echo "Installing drush..."
  if [ ! -d ${DRUSH_DIR} ]; then
    mkdir ${DRUSH_DIR}
  fi

  php -r "readfile('http://files.drush.org/drush.phar');" > ${DRUSH_DIR}/drush
  chmod 744 ${DRUSH_DIR}/drush
  ${DRUSH_DIR}/drush init -y
  popd &> /dev/null
fi

echo ''
echo 'Now reload your shell configuration (source ~/.[YOURCONFIG])'

