som_get-instance-alias () {
  pushd ~/.drush &> /dev/null
  echo $(ls -1 *.aliases.drush*.php) | sed -E 's/.aliases\.drush(rc)?\.php//'
  popd &> /dev/null
}

som_webroot () {
  echo ~/htdocs
}

