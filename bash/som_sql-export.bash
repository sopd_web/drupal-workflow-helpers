som_sql-export () {
  pushd $(som_webroot) &> /dev/null
  drush sql-dump > ~/$(som_get-instance-alias)-default.sql
  popd &> /dev/null
}

alias sse='som_sql-export'

