# Others
alias l='ls -lah'

# Git related aliases
alias g='git'
alias gb='git branch'
alias gss='git status'
alias grhh='git reset HEAD --hard'
alias gco='git checkout'
alias gc='git commit'
alias gaa='git add -A'
alias ga='git add'
alias gl='git pull'
alias gp='git push'
alias gr='git remote'
alias gd='git diff'
alias glog="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias gss='git status'

# Drush
alias dcr='drush cache-rebuild || drush cache-clear all'

