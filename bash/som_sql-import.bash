som_sql-import () {
  pushd $(som_webroot) &> /dev/null
  drush sql-cli < ~/$(som_get-instance-alias)-default.sql
  dcr
  popd &> /dev/null
}

alias ssi='som_sql-import'

