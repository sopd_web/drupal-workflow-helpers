som_file-sync () {
  pushd $(som_webroot)
  drush rsync --mode='akz --delete' @$(som_get-instance-alias).${1-integration}:sites/default/files default:sites/default/files
  popd
}

alias sfs='som_file-sync'

