som_sql-sync () {
  pushd $(som_webroot) &> /dev/null
  drush sql-sync \
    --source-dump=/tmp/dump.sql.gz \
    --structure-tables-list=key_value_expire,past_event,past_event_data,past_event_argument \
    --target-dump=/tmp/lt.sql.gz \
    @$(som_get-instance-alias).${1-integration} \
    default \
  || \
  drush -y @agvs.integration sql-dump | drush -y sql-cli
  popd &> /dev/null
}

alias sss='som_sql-sync'

