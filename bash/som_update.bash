# This function is mainly for internal use here
som_git-discard () {
  git reset --hard HEAD # Discard all changes
  git clean -df # Remove all uncommited files and directories
}

# This function should be run every time you merge with `develop`
som_update () {
  cd ~/htdocs
  som_git-discard # Discard all changes
  git pull # Pull from remote (origin)
  git submodule foreach 'git reset --hard HEAD && git clean -df ' # Discard all changes in submodules
  git submodule update # Update all submodules
  drush config-import # Import all pulled config files
  dcr # Rebuild all caches
}

alias sup='som_update'

